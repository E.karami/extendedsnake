import random
from functools import lru_cache
import copy
from copy import deepcopy
import json


def set_constants(c_json):
    global constant_dict
    constant_dict = c_json


class ModeledEnv:
    def __init__(self, copy=False):
        self.whose_turn = 0
        if not copy:
            self.state = ModeledState()


    def take_action(self, action, agent):
        result = self.state.update(action, agent)
        if result=="success" or 'has died' in result:
            still_alive=[snake.body!=[] for snake in self.state.agent_list]
            self.whose_turn = (self.whose_turn+1)%len(self.state.agent_list)
            while not still_alive[self.whose_turn]:
                self.whose_turn = (self.whose_turn+1)%len(self.state.agent_list)
        return result

    def perceive(self, agent):
        agent_data = self.state.agent_list[self.state.get_agent_index(agent)]
        return {
            "Current_Env": json.dumps(self, default=lambda o: o.__dict__, indent=4),
            "map": self.state.foodGrid,
            "chance map": self.state.chance_map,
            "agent body": agent_data.body,
            "score": agent_data.foodScore,
            "cost": agent_data.realCost,
            }

    def goal_test(self):
        for agent_idx in range(len(self.state.agent_list)):
            if self.state.get_team_score(agent_idx) >= self.state.winScore: return True
        return False

    def create_copy(self):
        return deepcopy(self)

    def get_current_state(self):
        for agent_idx in range(len(self.state.agent_list)):
            if self.state.get_team_score(agent_idx) >= self.state.winScore: break
            agent_idx=None

        return {
            'whose_turn': self.whose_turn,
            'whose_winner': agent_idx,
            'whose_alive': [snake.body!=[] for snake in self.state.agent_list],
            'what_team': [snake.team for snake in self.state.agent_list],
        }

    def __eq__(self, obj):
        return isinstance(obj, ModeledEnv) and \
               self.whose_turn == obj.whose_turn and\
               obj.state == self.state

    def __deepcopy__(self, memo):
        id_self = id(self)  # memoization avoids unnecesary recursion
        _copy = memo.get(id_self)
        if _copy is None:
            _copy = type(self)(copy=True)
            _copy.whose_turn == self.whose_turn
            _copy.state=deepcopy(self.state)

        return _copy


class ModeledState:
    def __init__(self,copy=False):
        if not copy:
            self.consume_tile, self.winScore = constant_dict['consume_tile'],\
                                               constant_dict['winScore']
            self.foodScoreMulti, self.foodAddScore, self.turningCost = constant_dict['foodScoreMulti'],\
                                                                       constant_dict['foodAddScore'],\
                                                                       constant_dict['turningCost']
            self.foodGrid = constant_dict['foodGrid'] if constant_dict['foodGrid'] is not None else None
            self.chance_map = constant_dict['chance_map'] if constant_dict['chance_map'] is not None else None
            self.agent_list=[]
            for i in range(len(constant_dict['agent_list'])):
                self.agent_list.append(ModeledSnake(i))

    def get_agent_index(self, agent):
        agent_idxs = [idx for idx, ad in enumerate(self.agent_list) if ad.agent_type == agent]
        if len(agent_idxs) == 0: print("agent not found"); return None
        else: return agent_idxs[0]

    def update(self, action, agent):
        agent_idx = agent if type(agent) is int else self.get_agent_index(agent)
        if agent_idx is None: return "invalid agent"
        if not self.validate_action(action): return "invalid action"
        snake = self.agent_list[agent_idx]

        if snake.body==[]: return snake.name+' has died'

        # move snake
        snake.move(action.lower())

        # check if impacted
        impact=self.check_for_impact(agent_idx)
        if impact == 'exited map' or impact=='hit its own body' or impact=='hit other snake body':
            snake.body=[]
            return snake.name+' has died'
        elif impact is not False: snake.foodScore -= 1

        # eat
        self.eat(agent_idx)

        # turning cost
        if snake.currentDir != action: snake.foodScore = max(snake.foodScore-self.turningCost, 0)
        snake.currentDir = action

        return "success"

    def validate_action(self, action):
        if type(action) is not str or action.lower() not in ["u", "d", "r", "l"]:
            print("the action '%s' is invalid" % action)
            return False
        return True

    def check_for_impact(self, agent_idx):
        snake = self.agent_list[agent_idx]
        snake_head = snake.body[-1]
        if not (0 <= snake_head[0] <= len(self.foodGrid) - 1 and 0 <= snake_head[1] <= len(self.foodGrid[0]) - 1):
            return 'exited map'
        for part in snake.body[:-1]:
            if snake_head == part: return 'hit its own body'

        for other_snake in self.agent_list:
            for part in other_snake.body:
                if snake is other_snake: continue
                if snake_head == part: return "hit other snake body"
        return False

    def eat(self, agent_idx):
        snake = self.agent_list[agent_idx]
        snake_head = snake.body[-1]

        if random.random() > self.chance_map[snake_head[0]][snake_head[1]]: return "nothing happened (stochastic)"

        tile=self.foodGrid[snake_head[0]][snake_head[1]]
        if len(snake.body) == 1 and snake.shekam == 0 and tile!=0:
            snake.foodScore += self.foodAddScore + self.foodScoreMulti * tile
            snake.shekam += tile

            if self.consume_tile:
                self.foodGrid[snake_head[0]][snake_head[1]] = 0
                # round(tile * random.uniform(0.1, 0.9))

    def get_team_score(self, agent_idx):
        snake = self.agent_list[agent_idx]
        score = 0
        for other_snake in self.agent_list:
            if other_snake.team == snake.team: score += other_snake.foodScore
        return score

    def __eq__(self, obj):
        return isinstance(obj, ModeledState) and \
            obj.agent_list == self.agent_list and \
            obj.foodGrid == self.foodGrid and \
            obj.chance_map == self.chance_map and \
            obj.foodAddScore == self.foodAddScore and \
            obj.winScore == self.winScore and \
            obj.turningCost == self.turningCost and \
            obj.consume_tile == self.consume_tile and \
            obj.foodScoreMulti == self.foodScoreMulti

    def __deepcopy__(self, memo):
        id_self = id(self)  # memoization avoids unnecesary recursion
        _copy = memo.get(id_self)
        if _copy is None:
            _copy = type(self)(copy=True)

            _copy.agent_list = []
            for snake in self.agent_list: _copy.agent_list.append(deepcopy(snake, memo))

            _copy.foodGrid = [[*a] for a in self.foodGrid]
            _copy.chance_map = self.chance_map
            _copy.foodAddScore = self.foodAddScore
            _copy.winScore = self.winScore
            _copy.turningCost = self.turningCost
            _copy.consume_tile = self.consume_tile
            _copy.foodScoreMulti = self.foodScoreMulti

            memo[id_self] = _copy
        return _copy


class ModeledSnake:
    def __init__(self, snake_idx, copy=False):
        self.snake_idx = snake_idx
        if not copy:
            self.name = constant_dict['agent_list'][self.snake_idx]['name']
            self.team = constant_dict['agent_list'][self.snake_idx]['team']
            #####################################
            self.agent_type = constant_dict['agent_list'][self.snake_idx]['agent_type']
            ########################################
            self.shekam = constant_dict['agent_list'][self.snake_idx]['shekam']
            self.foodScore = constant_dict['agent_list'][self.snake_idx]['foodScore']
            self.realCost = constant_dict['agent_list'][self.snake_idx]['realCost']
            self.currentDir = constant_dict['agent_list'][self.snake_idx]['currentDir']
            self.body = constant_dict['agent_list'][self.snake_idx]['body']

    def move(self, action):
        delta_i, delta_j = {"u": (-1,0), "d": (+1,0), "r": (0,+1), "l": (0,-1)}[action]
        self.body.append([self.body[-1][0]+delta_i, self.body[-1][1]+delta_j])

        if self.shekam == 0:
            del self.body[0]
            if len(self.body) != 1: del self.body[0]
        else: self.shekam -= 1

        self.realCost += 1

    def __eq__(self, obj):
        return isinstance(obj, ModeledSnake) and \
               obj.name == self.name and \
               obj.team == self.team and \
               obj.shekam == self.shekam and \
               obj.body == self.body and \
               obj.foodScore == self.foodScore and \
               obj.realCost == self.realCost and \
               obj.snake_type == self.agent_type and \
               obj.currentDir == self.currentDir

    def __deepcopy__(self, memo):
        id_self = id(self)  # memoization avoids unnecessary recursion
        _copy = memo.get(id_self)
        if _copy is None:
            _copy = type(self)(self.snake_idx,True)

            _copy.body = [list(part) for part in self.body]
            _copy.name = self.name
            _copy.team = self.team
            _copy.shekam = self.shekam
            _copy.foodScore = self.foodScore
            _copy.realCost = self.realCost
            _copy.currentDir = self.currentDir

            memo[id_self] = _copy
        return _copy