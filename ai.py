import random
from collections import deque
import json
from modeled_env import ModeledEnv, set_constants
from time import time
from queue import PriorityQueue


class Agent:
    def __init__(self, perceive_func=None, agent_id=None, optimized=True, mode='alpha_beta'):
        self.perceive_func = perceive_func
        self.my_id = agent_id

        self.predicted_actions = []
        self.actions_list = ['u', 'r', 'd', 'l']

        self.optimized = optimized
        self.alg = eval('self.'+mode)
        print('running '+mode)
    def act(self):
        # return ['up','down','right','left'][random.randint(0,3)]
        sensor_data = self.perceive_func(self)
        if self.optimized:
            set_constants(json.loads(sensor_data['Current_Env'])['state'])
            sensor_data['Current_Env'] = ModeledEnv()
        else:
            from env import Env
            self.actions_list = ['up', 'right', 'down', 'left']
            sensor_data['Current_Env'] = Env([1], [1]).from_json(**json.loads(sensor_data['Current_Env'])['state'])

        if self.predicted_actions == []:
            t0=time()
            self.predicted_actions = self.alg(sensor_data['Current_Env'])
            if self.optimized:
                self.predicted_actions = self.cast_actions()
            print(time()-t0)


        action = self.predicted_actions.pop()

        return action

    def cast_actions(self):
        d = {'u': 'up', 'd': 'down', 'l': 'left', 'r': 'right'}
        _copy = []
        for i in range(len(self.predicted_actions)):
            _copy.append(d[self.predicted_actions[i]])
        return _copy

    def evaluate(self,root_game):
        current = root_game.get_current_state()
        our_team_score=root_game.state.get_team_score(self.my_id)
        enemy_team_score=root_game.state.get_team_score(self.my_id ^ 1)
        our_alive_agents=0
        enemy_alive_agents=0
        for i in range(len(current['whose_alive'])):
            if current['what_team'][self.my_id]==current['what_team'][i] and current['whose_alive'][i]:
                our_alive_agents+=1
            if current['what_team'][self.my_id]!=current['what_team'][i] and current['whose_alive'][i]:
                enemy_alive_agents+=1
        if enemy_alive_agents==0:
            return [int(current['what_team'][self.my_id]),our_team_score, None]
        if enemy_team_score+enemy_alive_agents>our_team_score+our_alive_agents or our_alive_agents==0 :
            return [int(current['what_team'][self.my_id]) ^ 1,our_team_score,None]
        else: return [int(current['what_team'][self.my_id]),our_team_score,None]

    def heuristic(self, state):
        return state.state.get_team_score(self.my_id)-state.state.agent_list[self.my_id].shekam-len(state.state.agent_list[self.my_id].body)

    def sorted_action_list(self,root_game,agent_index,version='max'):
        q=PriorityQueue()
        for action in self.actions_list:
            child_game = root_game.create_copy()
            current=root_game.get_current_state()
            if current['whose_alive'][agent_index]:
                new_child=child_game.create_copy()
                if 'has died' not in new_child.state.update(action,agent_index):
                    child_game.take_action(action, agent_index)
                    result=self.heuristic(child_game.create_copy())
                    q.put((result,action))
        p=[]
        while not q.empty():
            l=q.get()
            p.append(l[1])
        if version=='max':
            p.reverse()
        return p

    def min_max(self, root_game):
        depth=-1
        result=self.max_value(root_game,depth+1)
        print(result)
        return [result[2]]

    def max_value(self,root_game,depth):
        current = root_game.get_current_state()
        # Search for a living enemy
        flag1 = False
        for i in range(len(current['whose_alive'])):
            if current['whose_alive'][i] and current['what_team'][i] != current['what_team'][current['whose_turn']]:
                flag1 = True
                break
        if root_game.goal_test():
            winner_team=current['what_team'][current['whose_winner']]
            our_team_score=root_game.state.get_team_score(self.my_id)
            return [winner_team, our_team_score, None]
        # The greater the depth, the better the answers, but the slower the time
        if depth>=8:
            return self.evaluate(root_game)
        if self.optimized:
            my_result=[None,float('-inf'),'r']
        else: my_result=[None,float('-inf'),'right']
        for action in self.sorted_action_list(root_game,current['whose_turn']):
            child_game=root_game.create_copy()
            ll=child_game.take_action(action,current['whose_turn'])
            next_current=child_game.get_current_state()
            if current['what_team'][self.my_id]==next_current['what_team'][next_current['whose_turn']]:
                next_mode='self.max_value'
            else:
                next_mode='self.min_value'
            if not flag1:
                next_mode = 'self.max_value'
            result=eval(next_mode)(child_game,depth+1)
            if result[0]==current['what_team'][current['whose_turn']]:
                if my_result[0]!=result[0]:
                    my_result[0]=result[0]
                    my_result[1]=result[1]
                    my_result[2]=action
                else:
                    if result[1]>my_result[1]:
                        my_result[1] = result[1]
                        my_result[2] = action
            else:
                if my_result[0]!=current['what_team'][current['whose_turn']] and result[1]>my_result[1]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action

        return my_result

    def min_value(self,root_game,depth):
        current = root_game.get_current_state()
        flag1=False
        for i in range(len(current['whose_alive'])):
            if current['whose_alive'][i] and current['what_team'][i]!=current['what_team'][current['whose_turn']]:
                flag1=True
                break
        if root_game.goal_test():
            winner_team = current['what_team'][current['whose_winner']]
            our_team_score = root_game.state.get_team_score(self.my_id)
            return [winner_team, our_team_score, None]
        # The greater the depth, the better the answers, but the slower the time
        if depth>=8:
            return self.evaluate(root_game)
        my_result = [None, float('inf'), None]
        for action in self.sorted_action_list(root_game, current['whose_turn'],version='min'):
            child_game = root_game.create_copy()
            ll=child_game.take_action(action, current['whose_turn'])
            next_current = child_game.get_current_state()
            if current['what_team'][self.my_id] == next_current['what_team'][next_current['whose_turn']]:
                next_mode = 'self.max_value'
            else:
                next_mode = 'self.min_value'
            if not flag1:
                next_mode = 'self.min_value'
            result = eval(next_mode)(child_game,depth+1)
            if result[0] == current['what_team'][current['whose_turn']]:
                if my_result[0] != result[0]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action
                else:
                    if result[1] < my_result[1]:
                        my_result[1] = result[1]
                        my_result[2] = action
            else:
                if my_result[0] != current['what_team'][current['whose_turn']] and result[1] < my_result[1]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action

        return my_result

    def alpha_beta(self, root_game):
        depth = -1
        result = self.max_value_ab(root_game, depth + 1,float('-inf'),float('inf'))
        print(result)
        return [result[2]]

    def max_value_ab(self,root_game,depth,alpha_limit,beta_limit):
        current = root_game.get_current_state()
        # Search for a living enemy
        flag1 = False
        for i in range(len(current['whose_alive'])):
            if current['whose_alive'][i] and current['what_team'][i] != current['what_team'][current['whose_turn']]:
                flag1 = True
                break
        if root_game.goal_test():
            winner_team = current['what_team'][current['whose_winner']]
            our_team_score = root_game.state.get_team_score(self.my_id)
            return [winner_team, our_team_score, None]
        # The greater the depth, the better the answers, but the slower the time
        if depth >= 7:
            return self.evaluate(root_game)
        if self.optimized:
            my_result = [None, float('-inf'), 'r']
        else:
            my_result = [None, float('-inf'), 'right']
        for action in self.sorted_action_list(root_game, current['whose_turn']):
            child_game = root_game.create_copy()
            ll = child_game.take_action(action, current['whose_turn'])
            next_current = child_game.get_current_state()
            if current['what_team'][self.my_id] == next_current['what_team'][next_current['whose_turn']]:
                next_mode = 'self.max_value_ab'
            else:
                next_mode = 'self.min_value_ab'
            if not flag1:
                next_mode = 'self.max_value_ab'
            result = eval(next_mode)(child_game, depth + 1,alpha_limit,beta_limit)

            # alpha beta pruning
            if result[0]==current['what_team'][self.my_id] and result[1]>=beta_limit:
                return result

            alpha_limit=max(result[1],alpha_limit)
            if result[0] == current['what_team'][current['whose_turn']]:
                if my_result[0] != result[0]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action
                else:
                    if result[1] > my_result[1]:
                        my_result[1] = result[1]
                        my_result[2] = action
            else:
                if my_result[0] != current['what_team'][current['whose_turn']] and result[1] > my_result[1]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action

        return my_result

    def min_value_ab(self,root_game,depth,alpha_limit,beta_limit):
        current = root_game.get_current_state()
        flag1 = False
        for i in range(len(current['whose_alive'])):
            if current['whose_alive'][i] and current['what_team'][i] != current['what_team'][current['whose_turn']]:
                flag1 = True
                break
        if root_game.goal_test():
            winner_team = current['what_team'][current['whose_winner']]
            our_team_score = root_game.state.get_team_score(self.my_id)
            return [winner_team, our_team_score, None]
        # The greater the depth, the better the answers, but the slower the time
        if depth >= 7:
            return self.evaluate(root_game)
        my_result = [None, float('inf'), None]
        for action in self.sorted_action_list(root_game, current['whose_turn'], version='min'):
            child_game = root_game.create_copy()
            ll = child_game.take_action(action, current['whose_turn'])
            next_current = child_game.get_current_state()
            if current['what_team'][self.my_id] == next_current['what_team'][next_current['whose_turn']]:
                next_mode = 'self.max_value_ab'
            else:
                next_mode = 'self.min_value_ab'
            if not flag1:
                next_mode = 'self.min_value_ab'
            result = eval(next_mode)(child_game, depth + 1,alpha_limit,beta_limit)

            # alpha beta pruning

            if result[0]==current['what_team'][current['whose_turn']] and result[1]<=alpha_limit:
                return result

            beta_limit=min(result[1],beta_limit)
            if result[0] == current['what_team'][current['whose_turn']]:
                if my_result[0] != result[0]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action
                else:
                    if result[1] < my_result[1]:
                        my_result[1] = result[1]
                        my_result[2] = action
            else:
                if my_result[0] != current['what_team'][current['whose_turn']] and result[1] < my_result[1]:
                    my_result[0] = result[0]
                    my_result[1] = result[1]
                    my_result[2] = action

        return my_result
